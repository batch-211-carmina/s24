// console.log("Hello World!");

//exponent operator
let getCube = (2**2)*2;

console.log(`The cube of 2 is ${getCube}`);


//array destructuring
const address = ["258 Washington", "Ave NW", "California", "90011"];
const [addressNumber, streetName, city, zipcode] = address;

console.log(`I live at ${addressNumber} ${streetName}, ${city} ${zipcode}`);


//object destructuring
const animal = {
			name : "Lolong",
			type : "saltwater crocodile",
			weight : "1075 kgs",
			measurement: "20 ft 3 in."
		};

const {name, type, weight, measurement} = animal;
console.log(`${name} was a ${type}. He weighed ${weight} with a measurement of ${measurement}`);


//arrow functions and loop using forEach
const numbers = [1, 2, 3, 4, 5]	;

	numbers.forEach((num) =>{
		console.log(num);
	});


const reduceNumber = numbers.reduce((x,y) => {
	let i = 0;
	let iterate = 0;
	return x + y;

});
console.log(reduceNumber);


//JavaScript Classes

class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.age = age;
		this.breed = breed;
	}
};

const myDog = new Dog("Soul", 3, "Lhasa Apso");
		console.log(myDog);


